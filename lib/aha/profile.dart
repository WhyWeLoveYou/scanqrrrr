import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qrappp/aha/homepage.dart';


class ProfileP extends StatefulWidget {
  const ProfileP({super.key});

  @override
  State<ProfileP> createState() => _ProfilePState();
}

class _ProfilePState extends State<ProfileP> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text("ProfilePage",
        style: TextStyle(
          color: Colors.white,
        )),
      ),
      body: Column(
        children: [
          aaa("Nama", Icon(Icons.person), "Nama"),
          aaa("Nomor", Icon(Icons.telegram), "Nomor"),
          aaa("Email", Icon(Icons.email), ""),
          aaa("Gtau", Icon(Icons.safety_check),""),
          ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const HOMEPAGE()),
                );
              },
              child: const Text("Aha"))
        ],
      ),
    );
  }

  Widget aaa(String text, Icon icon, String caption) {
    return Padding(
          padding: const EdgeInsets.all(16.0),
          child: TextFormField(
            decoration: InputDecoration(
              icon: icon,
                hintText: "Masukan $text",
                helperText: "Sesuaikan dengan $caption anda",
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    width: 1,
                    color: Colors.blueAccent,
                  ),
                )),
          ),
        );
  }
}
